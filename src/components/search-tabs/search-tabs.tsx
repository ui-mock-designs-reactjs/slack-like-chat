import { MoreOutlined } from "@ant-design/icons";
import React from "react";
import "./search-tabs.scss";

const TABS = ["New", "In progress", "On hold", "Completed"];

export const SearchTabs = () => {
  return (
    <div className="search-tabs">
      {TABS.map((tab) => {
        return (
          <div key={tab} className={`tab ${tab === "New" ? "active" : ""}`}>
            {tab}
          </div>
        );
      })}
      <MoreOutlined />
    </div>
  );
};
