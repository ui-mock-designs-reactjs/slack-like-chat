import React from 'react';
import './header.scss';
import logo from '../../images/chat-app-logo.png';

export const Header = () => {
    return (
        <div className="header">
            <div className="logo">
                <img alt="logo" src={logo} />
            </div>
            <div className="profile-photo">
                <img alt="avatar" src="https://randomuser.me/api/portraits/women/21.jpg" />
            </div>
        </div>
    )
}


