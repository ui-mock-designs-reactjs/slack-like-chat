import React from 'react';
import './user-list-item.scss';

interface Props {
    id: number;
    first_name: string;
    last_name: string;
    gender: string;
    email: string;
}

export const UserListItem = (props: Props) => {
    const imageUrl = props.gender === "Male"
        ? `https://randomuser.me/api/portraits/men/${props.id}.jpg`
        : `https://randomuser.me/api/portraits/women/${props.id}.jpg`;
    return (
        <div className="user-list-item">
            <div className="avatar">
                <img alt="user-avatar" src={imageUrl} />
            </div>
            <div className="details">
                <div className="name">{props.first_name} {props.last_name}</div>
                <div className="email">{props.email}</div>
            </div>
        </div>
    )
}