import React from "react";
import { FilterOutlined, SearchOutlined } from "@ant-design/icons";
import "./search-bar.scss";

interface Props {
  filterUsers: Function;
}

export const SearchBar = (props: Props) => {
  return (
    <div className="search-bar">
      <div className="search-box">
        <SearchOutlined />
        <input
          onChange={(e) => props.filterUsers(e.target.value)}
          placeholder="Search user"
          spellCheck="false"
          className="search-field"
          type="text"
        />
      </div>
      <div className="filter-button">
        <FilterOutlined />
      </div>
    </div>
  );
};
