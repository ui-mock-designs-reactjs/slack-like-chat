// const MESSAGES = [
//     {
//         id: 1,
// from_user: {
//     id: 23,
//     first_name: 'Gaurav',
//     last_name: 'Saluja',
//     avatar_url: '',
// },
//         message: 'Some message text',
//         media: [
//             {
//                 id: 1,
//                 type: 'IMG',
//                 url: '',
//             }
//         ],
//         type: 'MESSAGE',
//         date: new Date(),
//     },
//     {
//         id: 2,
//         type: 'SEPARATOR',
//         date: new Date()
//     },
// ]

import { getRandomUser } from "../screens/app-left/SAMPLE_DATA";

import att1 from '../images/att1.jpeg';
import att2 from '../images/att2.jpeg';
import att3 from '../images/att3.jpeg';
import att4 from '../images/att4.webp';
import att5 from '../images/att5.jpeg';
import att6 from '../images/att6.jpeg';

const SAMPLE_MEDIA = [
    att1, att2, att3, att4, att5, att6
];

const USER1 = {
    id: 23,
    first_name: 'Lucy',
    last_name: 'Goldstein',
    avatar_url: 'https://randomuser.me/api/portraits/women/21.jpg',
};

const randomUser = getRandomUser();
const imageUrl = randomUser.gender === "Male"
    ? `https://randomuser.me/api/portraits/men/${randomUser.id}.jpg`
    : `https://randomuser.me/api/portraits/women/${randomUser.id}.jpg`;

const USER2 = {
    id: randomUser.id,
    first_name: randomUser.first_name,
    last_name: randomUser.last_name,
    avatar_url: imageUrl
};

const CHAT_USERS = [
    USER1, USER2
];

function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const SAMPLE_MESSAGES = [`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. `,
    `eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. `,
    `Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. `,
    `Neque porro quisquam est. `,
    `qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. `,
    `sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? `,
    `Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`,
    `At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. `,
    `Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus `,
    `omnis voluptas assumenda est, omnis dolor repellendus. `,
    `Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. `,
    `Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.`];

export const getRandomConversationMessages = (count: number) => {

    const messages = [];

    for(let i=0; i<count; i++) {
        const randomUserIndex = getRandomInt(0,1);
        const messageUser = CHAT_USERS[randomUserIndex];

        const randomMessageIndex = getRandomInt(0, SAMPLE_MESSAGES.length - 1);
        const randomMessage = SAMPLE_MESSAGES[randomMessageIndex];

        const messageHasMedia = getRandomInt(0,1);
        const randomMediaCount = getRandomInt(0,3);

        const media = [];
        if(messageHasMedia) {
            for(let j=0; j<randomMediaCount; j++) {
                const randomMediaIndex = getRandomInt(0,SAMPLE_MEDIA.length - 1);
                media.push(SAMPLE_MEDIA[randomMediaIndex]);
            }
        }

        messages.push({
            id: i+1,
            from_user: messageUser,
            message: randomMessage,
            media
        })
    }

    return {
        messages,
        otherUser: USER2,
        myUser: USER1,
    };

};

