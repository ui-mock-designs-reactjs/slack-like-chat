import React from 'react';
import { AppCenter } from '../app-center/app-center';
import { AppLeft } from '../app-left/app-left';
import { AppRight } from '../app-right/app-right';
import './chat-app.scss';

export const ChatApp = () => {
    return (
        <div className="chat-app">
            <AppLeft />
            <AppCenter />
            <AppRight />
        </div>
    )
}