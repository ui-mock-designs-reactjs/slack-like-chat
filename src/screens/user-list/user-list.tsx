import React from "react";
import { UserListItem } from "../../components/user-list-item/user-list-item";
import { iUser } from "../app-left/SAMPLE_DATA";
import "./user-list.scss";

interface Props {
  users: iUser[];
}

export const UserList = (props: Props) => {
  return (
    <div className="user-list">
      {props.users.map((user) => {
        return (
          <UserListItem
            key={user.id}
            email={user.email}
            id={user.id}
            gender={user.gender}
            first_name={user.first_name}
            last_name={user.last_name}
          />
        );
      })}
    </div>
  );
};
