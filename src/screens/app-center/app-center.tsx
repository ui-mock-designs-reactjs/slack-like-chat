import {
  ApartmentOutlined,
  CaretDownOutlined,
  ClockCircleOutlined,
  EllipsisOutlined,
  FileOutlined,
  PhoneOutlined,
  PushpinOutlined,
  RedoOutlined,
  SmileOutlined,
} from "@ant-design/icons";
import React from "react";
import whatsapp_logo from "../../images/whatsapp_logo.png";
import { getRandomConversationMessages } from "../../utility/generate-sample-conversation";
import "./app-center.scss";

const CONVERSATION = getRandomConversationMessages(50);

export const AppCenter = () => {
  return (
    <div className="app-center">
      <div className="header">
        <div className="name">
          <b>
            {CONVERSATION.otherUser.first_name}{" "}
            {CONVERSATION.otherUser.last_name}
          </b>
        </div>
        <div className="right-buttons">
          <div className="button">
            <div className="left">
              <RedoOutlined />
            </div>
            <div className="center">Progress</div>
            <div className="right">
              <CaretDownOutlined />
            </div>
          </div>
          <div className="button">
            <div className="left">
              <div className="red-dot" />
            </div>
            <div className="center">High</div>
            <div className="right">
              <CaretDownOutlined />
            </div>
          </div>
          <div className="button">
            <div className="left">
              <ApartmentOutlined />
            </div>
            <div className="center">Marketing</div>
            <div className="right">
              <CaretDownOutlined />
            </div>
          </div>
          <div className="button">
            <div className="left">
              <img alt="avatar" src="https://randomuser.me/api/portraits/women/21.jpg" />
            </div>
            <div className="center">You</div>
            <div className="right">
              <CaretDownOutlined />
            </div>
          </div>
          <div className="button">
            <EllipsisOutlined style={{ fontSize: 22 }} />
          </div>
        </div>
      </div>
      <div className="chat-list">
        {CONVERSATION.messages.map((message) => {
          const isMyMessage = message.from_user.id === CONVERSATION.myUser.id;
          return (
            <div
              key={message.id}
              className={`chat-message ${
                isMyMessage ? "my-message" : "other-message"
              }`}
            >
              <div className="from-user">
                <img alt="from user" src={message.from_user.avatar_url} />
              </div>
              <div className="message-details">
                <div className="media-list">
                  {message.media.map((media, index) => (
                    <div key={index} className="media-item">
                      <img alt="media attachment" src={media} />
                    </div>
                  ))}
                </div>
                <div className="message-text">{message.message}</div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="message-writer">
        <div className="send-message">
          <div className="message-field-container">
            <input placeholder="Type your message here" type="text" />
          </div>
          <div className="footer">
            <div className="icons">
              <div className="button whatsapp">
                <div className="left">
                  <img alt="icon" src={whatsapp_logo} />
                </div>
                <div className="right">
                  <CaretDownOutlined />
                </div>
              </div>
              <div className="icon-buttons">
                <PushpinOutlined />
              </div>
              <div className="icon-buttons">
                <SmileOutlined />
              </div>
              <div className="icon-buttons">
                <ClockCircleOutlined />
              </div>
              <div className="icon-buttons">
                <PhoneOutlined />
              </div>
              <div className="icon-buttons">
                <FileOutlined />
              </div>
            </div>
            <div className="send">
              <div className="send-button">Send</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
