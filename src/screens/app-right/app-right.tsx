import { CaretDownOutlined } from '@ant-design/icons';
import React from 'react';
import './app-right.scss';

export const AppRight = () => {
    return (
        <div className="app-right">
            <div className="header">
                <div className="left"><CaretDownOutlined /> Overview</div>
                <div className="right">View details</div>
            </div>
            <div className="user-profile-details">
                <img alt="avatar" src="https://randomuser.me/api/portraits/women/21.jpg" />
                <div className="name">Hanno Engelmann</div>
                <div className="phone">(025) 014-2029</div>
            </div>
            <div className="header-secondary">
                <div className="left">
                    <CaretDownOutlined />
                    Tasks
                    <span className="count">3</span>
                </div>
                <div className="right">Add</div>
            </div>
            <div className="check-list">
                <div className="check-item">
                    <div className="left"><input type="checkbox"></input></div>
                    <div className="right"> Validate Proof</div>
                </div>
                <div className="check-item">
                    <div className="left"><input type="checkbox"></input></div>
                    <div className="right">Profile Verification</div>
                </div>
                <div className="check-item">
                    <div className="left"><input type="checkbox"></input></div>
                    <div className="right">Attach Files</div>
                </div>
            </div>
            <div className="header-secondary">
                <div className="left">
                    <CaretDownOutlined />
                    Linked Conversations
                    <span className="count">2</span>
                </div>
                <div className="right">Add</div>
            </div>

            <div className="image-name">
                <div className="image-linked">
                    <img alt="avatar" src="https://randomuser.me/api/portraits/women/24.jpg" />
                    <div className="details">
                        <div className="name-linked">Hanno Engelmann <span className="date">Jan 28</span></div>
                        <div className="text">
                            Nemo enim ipsam voluptatem quia voluptas
                        </div>
                    </div>


                </div>

            </div>
            <div className="image-name">
                <div className="image-linked">
                    <img alt="avatar" src="https://randomuser.me/api/portraits/women/24.jpg" />
                </div>
                <div className="details">
                    <div className="name-linked">Hanno Engelmann <span className="date">Feb 12</span></div>
                    <div className="text">
                        omnis voluptas assumenda est, omnis dolor repellendus.         </div>
                </div>
            </div>
        </div>
    )
}