import React, { useState } from "react";
import { SearchBar } from "../../components/search-bar/search-bar";
import { SearchTabs } from "../../components/search-tabs/search-tabs";
import { UserList } from "../user-list/user-list";
import "./app-left.scss";
import { iUser, SAMPLE_USERS } from "./SAMPLE_DATA";

export const AppLeft = () => {
  const [users, setUsers] = useState<iUser[]>(SAMPLE_USERS);

  const filterUsers = (text: string) => {
    console.log("Filter: ", text);
    setUsers(
      SAMPLE_USERS.filter(
        (user) =>
          user.first_name.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          user.last_name.toLowerCase().indexOf(text.toLowerCase()) > -1
      )
    );
  };
  return (
    <div className="app-left">
      <SearchBar filterUsers={filterUsers} />
      <SearchTabs />
      <UserList users={users} />
    </div>
  );
};
