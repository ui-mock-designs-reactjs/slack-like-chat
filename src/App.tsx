import React from 'react';
import './App.css';
import { Header } from './components/header/header';
import { ChatApp } from './screens/chat-app/chat-app';

function App() {
  return (
    <div className="App">
      <Header />
      <ChatApp />
    </div>
  );
}

export default App;
